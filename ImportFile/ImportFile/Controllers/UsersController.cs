﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using ExcelDataReader;
using ImportFile.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ImportFile.Controllers
{
    public class UsersController : Controller
    {
        [HttpGet]
        public IActionResult Index(List<UsersModel> users = null)
        {
            users = users == null ? new List<UsersModel>() : users;
            return View(users);
        }
        [HttpPost]
        [Obsolete]
        public IActionResult Index(IFormFile file, [FromServices] IHostingEnvironment hostingEnvironment)
        {
            string fileName = $"{ hostingEnvironment.WebRootPath}\\files\\{file.FileName}";
            using (FileStream fileStream = System.IO.File.Create(fileName))
            {
                file.CopyTo(fileStream);
                fileStream.Flush();

            }
            var users = this.GetUserList(file.FileName);
            return Index(users);
        }
        private List<UsersModel> GetUserList(string fName)
        {
            List<UsersModel> users = new List<UsersModel>();
            var fileName = $"{Directory.GetCurrentDirectory()}{@"\wwwroot\files"}" + "\\" + fName;
            System.Text.Encoding.RegisterProvider(System.Text.CodePagesEncodingProvider.Instance);
            using (var stream = System.IO.File.Open(fileName, FileMode.Open, FileAccess.Read))
            {
                using (var reader = ExcelReaderFactory.CreateReader(stream))
                {
                    while (reader.Read())
                    {
                        users.Add(new UsersModel()
                        {

                            FullName = reader.GetValue(0).ToString(),
                            PhoneNumber = reader.GetValue(1).ToString(),
                            Email = reader.GetValue(2).ToString(),
                            FirstName = reader.GetValue(3).ToString(),
                            LastName = reader.GetValue(4).ToString(),
                            ExternalEmail = reader.GetValue(5).ToString(),
                            Account = reader.GetValue(6).ToString(),
                            Password = reader.GetValue(7).ToString(),
                            Gender = reader.GetValue(8).ToString(),
                            DateOfBirth = reader.GetValue(9).ToString(),
                            University = reader.GetValue(10).ToString(),
                            Faculty = reader.GetValue(11).ToString(),
                            Skill = reader.GetValue(12).ToString()
                        });
                    }
                }
            }
            return users;
        }

    }
}
