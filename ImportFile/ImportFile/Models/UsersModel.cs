﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ImportFile.Models
{
    public class UsersModel
    {
        public string FullName { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string ExternalEmail { get; set; }
        public string Account { get; set; }
        public string Password { get; set; }
        public string Gender { get; set; }
        public String DateOfBirth { get; set; }
        public string University { get; set; }
        public string Faculty { get; set; }
        public string Skill { get; set; }
    }
}
